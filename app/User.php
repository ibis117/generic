<?php

namespace App;

use DataAccessPermission;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function IsDataAccessible(){
        $data_office_id = $this->office_id;
        // $data_office_id = $this->data["entry"]["office_id"];
        if ($data_office_id == 1 && $this->dataAccessPermission == DataAccessPermission::EnableEditButtonOnSystemData) {
            //do nothing
        } else {
            if ($data_office_id != $this->office_id) {
                return false;
                // return trans('auth.data_access_fail');
            }
        }
        return true;
    }

    public function assignRoleCustom($role_name){
        $roleModel = Role::where('name', $role_name)
        // ->orderBy('name', 'desc')
        ->take(1)
        ->get();
        
        if(count($roleModel) == 0){
            return "role doesnot exists";
        }
        DB::table('model_has_roles')->insert([
            'role_id' => $roleModel[0]->id,
            'model_type' => 'App\Models\BackpackUser',
            'model_id' => $this->id
        ]);
    }

    public function isSubOfficeUser(){
        if($this->office_wise_login == true || $this->unit_type_wise_login == true){
            return true;
        }
        return false;
    }
    public function isOfficeUser(){
        if(isset($this->office_id) && $this->office_id >= 1)
            return true;
        else {
            return false;
        }
    }
    public function isSystemUser(){
        return $this->isOfficeUser()?false:true;
    }
    public function roles(): MorphToMany
    {
        $superadmin_role = Role::where('name', 'superadmin')
        ->first();
        // dd($superadmin_role);
        // $this->query->where("name",'<>','superadmin');
        return $this->morphToMany(
            // 'App\Models\ClientRole',
            config('permission.models.role'),
            'model',
            config('permission.table_names.model_has_roles'),
            config('permission.column_names.model_morph_key'),
            'role_id'
        );
        //->wherePivot("role_id","<>", $superadmin_role->id);
        // $rel = $this->morphToMany(
        //     config('permission.models.role'),
        //     'model',
        //     config('permission.table_names.model_has_roles'),
        //     config('permission.column_names.model_morph_key'),
        //     'role_id'
        // );

        // // $rel->wherePivot("name","<>", "superadmin");
        // return $rel;
    }
    public function getDisabled(){
        if(isset($this->disabled))
            return $this->disabled;
        else
            return [];
    }
    public function setDisabled(array $fields = array()){
        if(isset($this->fillable) == false){
            $this->fillable = [];
        }
        $this->disabled = $fields;
        if(isset($fields) && is_array($fields)){
            foreach($fields as $key=>$value){
                $f_key = array_search($value, $this->fillable); 
                if(isset($f_key) && $f_key >= 0){
                    unset($this->fillable[$f_key]);
                }
            }
        }
    }
    // public function allowedOffice(){
    //     return $this->belongsTo('App\Models\OrgStructure', 'allowed_office_id' , 'id');
    // }
    // public function allowedUnitType(){
    //     return $this->belongsTo('App\Models\OrgUnitType', 'allowed_unit_type_id' , 'id');
    // }

    public static function getSystemUserId(){
        return 1;
    }

    public function isApplicant()
    {
        return $this->hasRole('applicant');
    }
}
