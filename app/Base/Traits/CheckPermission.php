<?php
namespace App\Base\Traits;

use App\Base\DataAccessPermission;



/**
 *  CheckPermission
 */
trait CheckPermission
{
    private $dataPermission;
    private $user;
    private $hasOfficeId;

    protected $permissions = ['list', 'create', 'update', 'delete', 'export', 'print'];
    protected $overRide = [];

    public function checkPermission($overRide =  [])
    {
        $this->overRide = $overRide;
        $this->dataPermission =  property_exists($this->crud->model, 'dataAccessPermission') ? $this->crud->model->dataAccessPermission : DataAccessPermission::SystemOnly;
        $this->user = backpack_user();

        $this->hasOfficeId = property_exists($this->crud->model, 'office_id');
        $this->hasOfficeId = in_array('office_id', $this->crud->model->getFillable());
        if ($this->hasOfficeId) {
            $this->filterByOffoce();
        }
        $this->crud->denyAccess(['list', 'create', 'update', 'delete', 'export', 'print']);
        $this->filterPermission();
    }


    private function filterByOffoce()
    {
        if (backpack_user()->hasrole('clientadmin')) {
            $this->crud->addClause('whereHas', 'office', function($q) {
                $user_office = backpack_user()->office;
                $q->where('provice_id', $user_office->provice_id);
            });
        }

        if (backpack_user()->hasrole('district_admin') 
        || backpack_user()->hasrole('operator')
        ) {
            $user_office = backpack_user()->office;
            $user_office_id = $user_office != null ? $user_office->id ?? 1 : 1;
            $this->crud->addClause('where', 'office_id', $user_office_id);
        }
    }


    public function filterPermission()
    {
        $data = [];
        switch($this->dataPermission) {
            case DataAccessPermission::DeveloperOnly:
                $data = $this->developerOnly();
            break;
            case DataAccessPermission::SystemOnly:
                $data = $this->systemOnly();
            break;
            case DataAccessPermission::ClientCRUDRestriction:
                $data = $this->clientCrudRestriction();
            break;
            case DataAccessPermission::SystemDataShareWithClient:
                $data = $this->systemDataSharedWithClient();
            break;
            case DataAccessPermission::ShowClientWiseDataOnly:
                $data = $this->showClientWiseDataOnly();
            break;
            case DataAccessPermission::DisableClientView:
                $data = $this->disableClientView();
            break;
            case DataAccessPermission::EnableEditButtonOnSystemData:
                $data = $this->enableEditButtonOnSystemData;
            break;
            case DataAccessPermission::DisplayClientDataToSystemUser:
                $data = $this->displayClientDataToSystemUser();
            break;
        }
        $access = [];

        if (!empty($this->overRide)) {
            foreach ($this->overRide as $key => $value) {
                $data[$key] = $value;
            }
        }
        
        foreach (backpack_user()->getRoleNames() as $role) {
            $access = array_unique(array_merge($access, $data[$role])); 
        }

        $this->crud->allowAccess($access);
    }

    public function developerOnly()
    {
        return [
            'superadmin' => ['list', 'create'],
            'clientadmin' => ['list', 'create'],
            'district_admin' => ['list', 'create'],
            'operator' => ['list', 'create'],
            'applicant' => ['list', 'create'],
        ];
    }


    public function systemOnly()
    {
        return [
            'superadmin' => ['list', 'create', 'update', 'delete', 'export', 'print'],
            'clientadmin' => ['list', 'export', 'print'],
            'district_admin' => ['list', 'export', 'print'],
            'operator' => ['list',  'print'],
            'applicant' => ['list', 'create'],
        ];  
    }

    public function showClientWiseDataOnly()
    {
        return [
            'superadmin' => ['list', 'create', 'update', 'delete', 'export', 'print'],
            'clientadmin' => ['list', 'export', 'print'],
            'district_admin' => ['list', 'export', 'print'],
            'operator' => ['list',  'print'],
            'applicant' => ['list', 'create'],
        ];  
    }    
}
