<?php
namespace App\Base\Traits;


/**
 * 
 */
trait ParentData
{
    

    public function parent($name)
    {
        $request = $this->crud->request;
        return $request->route($name) ?? null;
    }

    public function setUpLink($methods = ['index'])
    {
        $currentMethod = $this->crud->getActionMethod();
        $exits = method_exists($this, 'tabLinks');
        if ($exits && in_array($currentMethod, $methods)) {
            $this->data['tab_links'] = $this->tabLinks();
        }
    }
    

}
