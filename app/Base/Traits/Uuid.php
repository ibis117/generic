<?php
namespace App\Base\Traits;

use Webpatser\Uuid\Uuid as Webuuid;


/**
 * 
 */
trait Uuid
{
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Webuuid::generate()->string;
            if(in_array('code', $model->getFillable())){
                $code = self::generateCode($model);
                $model->code = $code;   
            }
            $model->created_by = backpack_user()->id;
        });
    }

    public static function generateCode($model)
    {
        $table = $model->getTable();
        $qu = DB::table($table)
                    ->selectRaw('COALESCE(max(code::NUMERIC),0)+1 as code')
                    ->whereRaw("(code ~ '^([0-9]+[.]?[0-9]*|[.][0-9]+)$') = true");
                    // ->where('deleted_uq_code',1);
                if(in_array('office_id',$model->getFillable())){
                    $qu->where('office_id', backpack_user()->office_id);
                }
                $rec = $qu->first();
                if(isset($rec)){
                    $code = $rec->code;
                }
                else{
                    $code = 1;
                }
                return $code;
    }
}
