<?php
namespace App\Base;

use App\Base\Traits\Uuid;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use Uuid;

    public $incrementing = false;
    protected $keyType= "string";

}
