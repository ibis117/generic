# Base

Base code structure to follow in projects



## Setup
```sh
$ git clone https://gitlab.com/gas_projects/gas_base.git <projectname>
$ cd <projectname>
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate
```


# How to create module

Following package is used for module [Click here](https://nwidart.com/laravel-modules/v6/introduction)

```sh
$ php artisan module:make <ModuleName>
$ php artisan module:make-migration create_posts_table <ModuleName>
$ php artisan module:make-controller PostController <ModuleName>
$ php artisan module:mkae-model Post <ModuleName>
```

