<div class="row">
    <div class='col-md-12' style="margin:10px;">
            <ul class="nav nav-tabs flex-column flex-sm-row"id="myTab" role="tablist">
                @foreach ($links as $tab)   
                @php
                    $active = url($tab['link']) == url()->current() ? 'active': '';
                @endphp
                    <li role="presentation" class="nav-item {{ $active }}">
                    <a class="nav-link {{ $active }}" href="{{ url($tab['link']) }}" role="tab" >{{ $tab['label'] }} </a>
                    </li>
                @endforeach
            </ul>
    </div>
</div>
