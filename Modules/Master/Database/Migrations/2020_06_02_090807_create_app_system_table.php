<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_office_type', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code')->nullable();
            $table->string('name_en',200);
            $table->string('name_lc',200);
            $table->string('remarks',1000)->nullable();
            $table->boolean('is_active')->nullable();

            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->boolean('is_deleted')->nullable();
            $table->unsignedInteger('deleted_uq_code')->nullable()->default(1);

            $table->unique(['code','deleted_uq_code'],'uq_app_office_type_code');
            $table->unique(['name_lc','deleted_uq_code'],'uq_app_office_type_name_lc');
            $table->unique(['name_en','deleted_uq_code'],'uq_app_office_type_name_en');

        });

        Schema::create('app_office', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code')->nullable();
            $table->string('name_en',200);
            $table->string('name_lc',200);
            $table->string('admin_email',200);
            $table->uuid('office_type_id');
            $table->uuid('provice_id')->nullable();
            $table->uuid('district_id')->nullable();
            $table->uuid('local_level_id')->nullable();
            $table->string('ward_number',10)->nullable();
            $table->string('street_name',200)->nullable();
            $table->string('house_number',200)->nullable();
            $table->string('phone',200)->nullable();
            $table->string('fax',200)->nullable();
            $table->string('email',200)->nullable();
            $table->string('url',200)->nullable();
            $table->string('remarks',1000)->nullable();
            $table->boolean('is_active')->nullable();

            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->boolean('is_deleted')->nullable();
            $table->unsignedInteger('deleted_uq_code')->nullable()->default(1);

            $table->unique(['code','deleted_uq_code'],'uq_app_office_code');
            $table->unique(['name_lc','deleted_uq_code'],'uq_app_office_name_lc');
            $table->unique(['name_en','deleted_uq_code'],'uq_app_office_name_en');
            $table->foreign('office_type_id','fk_app_office_office_type_id')->references('id')->on('app_office_type');
            // $table->foreign('provice_id','fk_app_office_provice_id')->references('id')->on('mst_fed_province');
            // $table->foreign('district_id','fk_app_office_district_id')->references('id')->on('mst_fed_district');
            // $table->foreign('local_level_id','fk_app_office_local_level_id')->references('id')->on('mst_fed_local_level');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_office');
        Schema::dropIfExists('app_office_type');
    }
}
