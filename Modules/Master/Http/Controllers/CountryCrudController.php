<?php

namespace Modules\Master\Http\Controllers;

use App\Base\BaseCrudController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Master\Entities\Country;

class CountryCrudController extends BaseCrudController
{
    public function setup()
    {
        $this->crud->setModel('Modules\Master\Entities\Country');
        $this->crud->setRoute('master/country');
        $this->crud->setEntityNameStrings('country', 'देश');
        // if($this->crud->request->get("async") == "async"){
            // $this->enableDialog(true);
        // }
        $this->crud->addFilter(
            [
                'type' => 'text',
                'name' => 'name_en',
                'label' => 'Name'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'name_en', 'ILIKE', "%$value%");
            }
        );
        $this->crud->addFilter(
            [
                'type' => 'text',
                'name' => 'name_lc',
                'label' => 'नाम'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'name_lc', 'ILIKE', "%$value%");
            }
        );
    }

    protected function setupListOperation()
    {
        $col = [
            $this->addRowNumber(),
            $this->addCodeColumn(),
            [
                'name' => 'name_lc',
                'type' => 'text',
                'label' => trans('country.name_lc'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],
            [
                'name' => 'name_en',
                'type' => 'text',
                'label' => trans('country.name_en'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],
         
    
            ];
        $this->crud->addColumns($col);
    }


    protected function setupCreateOperation()
    {
        // $this->crud->setValidation(CountryRequest::class);

        $arr=[
         
            $this->addCodeField(),
            [
                'name' => 'name_en',
                'type' => 'text',
                'label' => trans('country.name_en'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],

            [
                'name' => 'name_lc',
                'type' => 'text',
                'label' => trans('country.name_lc'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6',
                ],
            ],
            $this->addRemarksField(),
        ];
        $this->crud->addFields($arr);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    // protected function setupCustomRoutes($segment, $routeName, $controller)
    // {
    
    //     \Illuminate\Support\Facades\Route::get($segment.'/{id}/custom/{custom_id}', [
    //         'as'        => $routeName.'.custom',
    //         'uses'      => $controller.'@custom',
    //         'operation' => 'custom',
    //     ]);
    // }

    // public function custom($id, $custom_id)
    // {
    //     dd($id, $custom_id);
    // }

}
