<?php

namespace Modules\Master\Http\Controllers;

use App\Base\BaseCrudController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class AppOfficeTypeCrudController extends BaseCrudController
{
    public function setup()
    {
        $this->crud->setModel('Modules\Master\Entities\AppOfficeType');
        $this->crud->setRoute('master/officetype');
        $this->crud->setEntityNameStrings('appofficetype', trans('appOfficeType.title_text'));
    }

    protected function setupListOperation()
    {
        $col=[
            // $this->addCodeColumn(),
            $this->addRowNumber(),
            [
                'name'=>'name',
                'label'=>trans('appOfficeType.name_lc'),
                'type'=>'model_function',
                'function_name'=>'name'
            ],
            [
                'name'=>'is_active',
                'type'=>'radio',
                'label'=>trans('appOfficeType.is_active'),
                'options'=>[
                    '1'=>'हो',
                    '0'=>'होइन'
                ],
            ]
        ];
        $this->crud->addColumns($col);
    }

    protected function setupCreateOperation()
    {
        // $this->crud->setValidation(AppOfficeTypeRequest::class);
        $arr=[
            // $this->addCodeField(),
            [
                'name'=>'name_lc',
                'type'=>'text',
                'label'=>trans('appOfficeType.name_lc'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ],
            ],
            [
                'name'=>'name_en',
                'type'=>'text',
                'label'=>trans('appOfficeType.name_en'),
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ],
            ],
            [  
                'name'        => 'is_active', 
                'label'       => trans('appOffice.is_active'), 
                'type'        => 'radio',
                'options'     => [
                    0 => "होइन",
                    1 => "हो"
                ],
                'inline'      => true, 
            ],
             // $this->addRemarksField(),
        ];
        $this->crud->addFields($arr); 
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
