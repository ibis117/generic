<?php

namespace Modules\Master\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Base\BaseCrudController;

class MasterController extends BaseCrudController
{
    public function setup()
    {
        $this->crud->setModel('App\Models\Tag');
        $this->crud->setRoute('/master');
        $this->crud->setEntityNameStrings('master', 'masters');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(TagRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
