<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

// Route::group([
//     // 'prefix'     => config('backpack.base.route_prefix', 'admin'),
//     'middleware' => ['web'],
//     'namespace'  => 'Modules\Master\Http\Controllers',
// ], function () { // custom admin routes
//     Route::crud('master', 'MasterController');
// }); // this should be the absolute last line of this file