<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('admin/master')->group(function() {

// });

Route::group([
    'prefix'     => 'master',
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
], function () { // custom admin routes
    Route::crud('/', 'MasterController');
    Route::crud('/officetype', 'AppOfficeTypeCrudController');
    Route::crud('/country', 'CountryCrudController');
});
