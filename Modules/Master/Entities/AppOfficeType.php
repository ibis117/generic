<?php

namespace Modules\Master\Entities;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class AppOfficeType extends BaseModel
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'app_office_type';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['code','name_en','name_lc','remarks','is_active'];
    // protected $hidden = [];
    // protected $dates = [];

    public function name()
    {
        return $this->name_lc.'<br>'.$this->name_en;
    }
}
