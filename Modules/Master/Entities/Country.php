<?php

namespace Modules\Master\Entities;

use App\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class Country extends BaseModel
{
    use CrudTrait;

    protected $table = 'mst_country';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
}
